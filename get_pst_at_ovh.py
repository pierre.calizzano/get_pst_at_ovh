#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    rewrite in native python of the script get_pst_at_ovh.tag
    version 1.1.2
"""
import sys
import os
import datetime
import platform
import enum
import getpass
import traceback
import rpa as r
import xml.etree.ElementTree as xet
import requests
import pczMiscLib as myLib
import pczMailer
import pczLogger

# from multiprocessing.pool import ThreadPool
from multiprocessing import Process


class Language(enum.Enum):
    FR = 1
    EN = 2


SCRIPTNAME = "get_pst_at_ovh"
LOG_FILENAME = f"./{SCRIPTNAME}.log"
CONFIG_PATH = f"./{SCRIPTNAME}.config.xml"
g_xDocConfig = xet.parse(CONFIG_PATH)
g_xRoot = g_xDocConfig.getroot()


def main():
    try:
        rc = -1
        bDoInit = False
        arEmailBody = [" "]
        myMailer = pczMailer.PczMailer()
        myMailer.initFromXml(g_xRoot.find("./mailer").get("configPath"))

        sStartingDate = datetime.datetime.today().strftime("%d-%m-%Y")
        sStartingTime = datetime.datetime.today().strftime("%H:%M:%S")
        sHostName = platform.node()
        arEmailBody.append(
            f"Compte rendu du script '{SCRIPTNAME}' executé le {sStartingDate} à {sStartingTime} sur {sHostName}"
        )

        # get startup parameters
        if len(sys.argv) != 2:
            # invalid => help
            # raise myLib.PczException("Need the PASSWORD of OVH account")  #cf. /gitlab/issues/#11
            if g_xRoot.find("./config/password") is not None:
                ovh_pwd = g_xRoot.find("./config/password").text
            else:
                ovh_pwd = ''
                while len(ovh_pwd) == 0:
                    print("Enter OVH password (or 'Q' to Quit) ")
                    ovh_pwd = getpass.getpass()
                if ovh_pwd.upper() == "Q":
                    raise myLib.PczException("Program Quit by user")
        else:
            ovh_pwd = sys.argv[1]
        ovh_id = g_xRoot.find("./config/loginId").text
        sPathToSave = g_xRoot.find("./config/pathToSave").text

        # start tagUI
        bDoInit = rpaInit(r)
        if not bDoInit:
            raise RpaException("Error on init()")
        iDefaultTimeOut = 60
        if g_xRoot.find("./config/timeout") is not None:
            iDefaultTimeOut = int(g_xRoot.find("./config/timeout").text)
        r.timeout(iDefaultTimeOut)  # set the max time to wait for element

        # login to ovh console
        bResult: bool
        eLang: Language
        bResult, eLang = rpaLogIn(r, ovh_id, ovh_pwd)
        if not bResult:
            raise RpaException("Error on rpaLogIn()")

        bResult = rpaShowListEmails(r, eLang)
        if not bResult:
            raise RpaException("Error on rpaShowListEmails()")

        # For each mail account declared in the config.xml file
        arPst = []  # The list of processes used to download .PST files
        for xCompte in g_xRoot.findall("./comptes/compte"):
            sEmail = xCompte.find("./ovh_email").text
            urlPst = rpaGetPst(r, sEmail, xCompte.find("./ovh_filter").text)
            if urlPst is None:
                g_logger.error(f"An error occured for '{sEmail}' account")
            else:
                g_logger.debug(f"Got a valid URL for {sEmail} account.")
                sDateTime = datetime.datetime.today().strftime("%Y%m%d_%H%M%S")
                sFileName = (
                    f"{sPathToSave}/{xCompte.find('./file_name').text}-{sDateTime}.pst"
                )
                oPst = PstFile(a_email=sEmail, a_url=urlPst, a_pathToSave=sFileName)
                oPst.download()  # start the download process
                arPst.append(oPst)

        rpaLogOff(r, eLang)  # disconnect from ovh.com
        rpaClose(r)  # stop browser automation
        bDoInit = False

        g_logger.debug("Waiting for all download...")
        for pst in arPst:
            g_logger.debug(f"First chance for {pst.email} .PST file")
            pst.process.join()  # Wait for end of process
            g_logger.debug(
                f"The process to download .PST file of  {pst.email} as stopped running"
            )
            if not pst.check():
                g_logger.error(f".PST file is not valid for  {pst.email} account !")
                pst.download()  # restart download
        # new check for a second chance for invalid files (some time, it works with ovhcloud web site to give a second try!)
        g_logger.debug("Second chance for downloads not valid...")
        for pst in arPst:
            pst.process.join()  # Wait for end of process
            # g_logger.debug(f"The process to download .PST file of  {pst.email} as stopped running")
            if not pst.check():
                g_logger.error(
                    "The download for {pst.email} is an INVALIDE file => Do a manual download if needed"
                )
            sMessage = f". for {pst.email}, the file is downloaded in '{pst.path}' with a size of {pst.size} bytes ({pst.size / 1024000} Mo)."
            g_logger.info(sMessage)
            arEmailBody.append(sMessage)

        g_logger.debug("All download are terminated.")
        ## Fin du script
        rc = 0

    except RpaException as e:
        g_logger.error(f"RpaException: {e.message}")
        arEmailBody.append(f"EXCEPTION: RPA error : {e.message}")
        if bDoInit:
            r.snap("page", "./RpaErrorScreenCapture.png")
            r.close()  # stop browser automation
        rc = 2
    except myLib.PczException as e:
        g_logger.error(f"PczException: {e.message}")
        arEmailBody.append(f"EXCEPTION: Fatale error : {e.message}")
        if bDoInit:
            r.close()  # stop browser automation
        rc = 1
    except Exception as e:
        g_logger.error(f"EXCEPTION: Fatale error '{e}' - " + traceback.format_exc())
        arEmailBody.append(f"EXCEPTION: Fatale error : '{e}' (see log for details)")
        if bDoInit:
            r.close()  # stop browser automation
        rc = -1
    finally:
        arEmailBody.append(f"Ending script with ReturnCode= {rc}")
        sMessage: str = "\n".join(arEmailBody)
        if rc == 0:
            myMailer.addMail(
                sender=g_xRoot.find("./mailer").get("from"),
                to=g_xRoot.find("./mailer").get("to"),
                subject=f"Script {SCRIPTNAME}, execution report.",
                message=sMessage,
                attachments=LOG_FILENAME,
            )
        else:
            myMailer.addMail(
                sender=g_xRoot.find("./mailer").get("from"),
                to=g_xRoot.find("./mailer").get("to"),
                subject=f"Script {SCRIPTNAME}, execution report.",
                message=sMessage,
            )
        return rc
    # EndOf main()


def checkPopupRgpg(a_r):
    """Check if GDPR popup is displayed"""
    g_logger.debug("checkPopupRgpg()")
    arTextPopupRgpd = {
        "FR": "La protection de vos données est",
        "EN": "Protecting your data is our priority",
    }
    arTextButtonRgpd = {"FR": "Accepter", "EN": "Accept"}

    sScreenText = a_r.text()
    while len(sScreenText) == 0:
        a_r.wait(1.0)
        sScreenText = a_r.text()
    for lg in Language:
        if arTextPopupRgpd[lg.name] in sScreenText:
            # The RGPD popup is displayed
            g_logger.debug("popup RGPD  detectee => ")
            a_r.click(arTextButtonRgpd[lg.name])
            a_r.wait(1.0)
            return True
    return False
    # endOf checkPopupRgpg()


def detectLanguageOfSite(a_r):
    """What is the display language of the OVH site ?"""
    arTextLanguage = {"FR": "Français", "EN": "English"}

    sScreenText = a_r.text()
    while len(sScreenText) == 0:
        a_r.wait(1.0)
        sScreenText = a_r.text()
    for lg in Language:
        if arTextLanguage[lg.name] in sScreenText:
            return lg
    return Language.FR
    # endOf detectLanguageOfSite()


def waitForExportPst(a_r):
    """Wait for the download link to be displayed"""
    g_logger.debug("waitForExportPst()")
    arTextPopup2: list[str] = {"FR": "Lancer l'exportation", "EN": "Launch the export"}
    arTextPopup6: list[str] = {"FR": "Génération du lien", "EN": "Generating the link"}
    arTextPopup8: list[str] = {
        "FR": "Votre fichier est maintenant disponible",
        "EN": "Your file is now available",
    }
    g_logger.debug("Wait for the download link to be displayed, Start Loop")
    for i in range(0, 1100, 1):
        sScreenText: str = a_r.text()
        for lg in Language:
            if arTextPopup8[lg.name] in sScreenText:
                g_logger.debug("File is ready for download  :)")
                return True
        # It is necessary to test the other popup that may appear
        for lg in Language:
            if arTextPopup2[lg.name] in sScreenText:
                g_logger.debug("[Starting Export...]")
                a_r.click(arTextPopup2[lg.name])  # button [Lancer l'exportation]
        for lg in Language:
            if arTextPopup6[lg.name] in sScreenText:
                g_logger.debug("[Link generation...]")
                a_r.click(arTextPopup6[lg.name])  # button [Génération du lien]
        a_r.wait(5.0)  # 1100 * 5s ~= 90 Mn
    # raise myLib.RpaException("Nothing find in waitForExportPst()")  # raise an exception if nothing find
    return False
    # endOf waitForExportPst()


class RpaException(Exception):
    """managed exception"""

    def __init__(self, message):
        # self.when = datetime.now()
        self.message = message
        super().__init__(self)

    def __str__(self):
        s = self.message
        return s

    # EndOf RpaException()


def rpaInit(a_r):
    """
    initialize tagUI contexte
    cf. issue #4
    """
    g_logger.debug("Init RPA context")
    if g_xRoot.find("./config/init/visual_automation") is not None:
        bVisualAutomation = myLib.convertStringToBool(
            g_xRoot.find("./config/init/visual_automation").text
        )
    else:
        bVisualAutomation = True
    if g_xRoot.find("./config/init/chrome_browser") is not None:
        bChromeBrowser = myLib.convertStringToBool(
            g_xRoot.find("./config/init/chrome_browser").text
        )
    else:
        bChromeBrowser = True
    if g_xRoot.find("./config/init/headless_mode") is not None:
        bHeadLess = myLib.convertStringToBool(
            g_xRoot.find("./config/init/headless_mode").text
        )
    else:
        bHeadLess = False
    if g_xRoot.find("./config/init/turbo_mode") is not None:
        bTurboMode = myLib.convertStringToBool(
            g_xRoot.find("./config/init/turbo_mode").text
        )
    else:
        bTurboMode = False

    if g_logger is not None:
        g_logger.debug(
            f"Start RPA context - init(visual_automation={bVisualAutomation}, chrome_browser={bChromeBrowser}, headless_mode={bHeadLess}, turbo_mode={bTurboMode})"
        )
    return a_r.init(
        visual_automation=bVisualAutomation,
        chrome_browser=bChromeBrowser,
        headless_mode=bHeadLess,
        turbo_mode={bTurboMode},
    )
    # EndOf rpaInit()


def rpaClose(a_r):
    """
    Close tagUI context
    cf. issue #4
    """
    if g_logger is not None:
        g_logger.debug("Close RPA context")
    a_r.close()  # stop browser automation
    # EndOf rpaClose()


def rpaShowListEmails(a_r, a_lang: Language):
    """
    All the tagUI steps to display the list of emails account
    Return False if something get wrong.
    """
    a_r.wait(5.0)
    # goto Exchange config
    g_logger.debug(f"click sur lien '{g_xRoot.find('./config/linkExchangeId').text}'")
    if not a_r.click(g_xRoot.find("./config/linkExchangeId").text):
        return False
    # goto emails accounts list
    g_logger.debug('click on link "Comptes e-mail"')
    bResult: bool
    if a_lang == Language.FR:
        bResult = a_r.click("Comptes e-mail")
    else:
        bResult = a_r.click("Email accounts")
    # edit 24/02/22 : replace "libel" by XPath
    if not bResult :
        bResult = a_r.click("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[1]/div/oui-header-tabs/nav/div/oui-header-tabs-item[3]/div/a/span/span")
    if not bResult:
        return False
    # The list of email accounts associated with Exchange is displayed
    a_r.wait(2.0)
    return True
    # EndOf rpaShowListEmails()


def rpaGetPst(a_r, a_ovhEmail, a_ovhFilter):
    """
    All the tagUI steps for getting url of .PST to download
    Start when the list of Emails account is displayed.
    Return the URL, None if something get wrong.
    """
    g_logger.info(f"Starting rpaGetPst({a_ovhEmail}, {a_ovhFilter})")

    # filter zone
    a_r.type(
        "/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[1]/div/div/exchange-account-home/div/oui-datagrid/oui-criteria/div/oui-search/form/input",
        f"[clear]{a_ovhFilter}",
    )
    a_r.wait(10.0)  # OVH display is not very fast, or it's my bandwidth ;-)
    # do action menu (...)
    if not a_r.click(
        "/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[1]/div/div/exchange-account-home/div/oui-datagrid/div[1]/table/tbody[1]/tr/td[9]/oui-datagrid-cell/div/oui-action-menu/oui-dropdown/button/span"
    ):
        return None
    # command "Exporter au format PST"
    if not a_r.click(
        "/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[1]/div/div/exchange-account-home/div/oui-datagrid/div[1]/table/tbody[1]/tr/td[9]/oui-datagrid-cell/div/oui-action-menu/oui-dropdown/oui-dropdown-content/oui-action-menu-item[4]/button/span"
    ):
        return None
    a_r.wait(5.0)
    bResult = waitForExportPst(a_r)
    if not bResult:
        g_logger.error("Error in waitForExportPst()")
        return None
    # The url is available
    urlPst = a_r.read(
        "/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div/div/div[2]/div[4]/a"
    )
    # g_logger.info(f"urlPst={urlPst}") !security fault:Don't log because the url is valid without authentification at ovh.com!
    # button [Annuler]
    a_r.click(
        "/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div/div/div[3]/button[1]"
    )
    return urlPst
    # EndOf rpaGetPst()


def rpaLogIn(a_r, a_ovhId: str, a_ovhPwd: str):
    """
    RPA step to connect to OVH console web site
    Return True/False and the detect language (tuple)
    """
    sUrl = "https://www.ovhcloud.com/fr/"
    g_logger.debug("Start RPA Log-in for " + sUrl)
    a_r.url(sUrl)
    a_r.wait(5.0)
    checkPopupRgpg(a_r)
    # click link "Mon compte client"
    g_logger.debug('click link "Mon compte client"')
    a_r.click(
        ".ods-header-topbar__content__menu__item.ods-header-topbar__content__menu__item--n1.d-lg-flex.align-items-lg-stretch.ods-header-topbar__content__menu__item--manager .ods-header-topbar__content__menu__item__link__title.flex-grow-1"
    )
    # Enter ID OVH
    a_r.type("/html/body/div[3]/div/div[1]/div[1]/form/div[1]/div/div/input", a_ovhId)
    # Enter PWD OVH
    a_r.type("/html/body/div[3]/div/div[1]/div[1]/form/div[2]/div/div/input", a_ovhPwd)
    # tempo pour 2FA
    g_logger.debug("Wait 8s for 2FA...")
    a_r.wait(8.0)
    # Button [Se connecter]
    if not a_r.click(".span5.offset1.login-inputs.login-inputs-login .btn"):
        return (False, Language.EN)
    # /!\ The language of the site can change between .FR and .EN . Depend on browser, session context ?
    eLang: Language = detectLanguageOfSite(a_r)
    # Sometimes the GDPR popup is displayed after a while
    a_r.wait(4.0)
    checkPopupRgpg(a_r)
    g_logger.debug("End of RPA Log-in")
    return (True, eLang)
    # EndOf rpaLogIn()


def rpaLogOff(a_r, a_lang):
    """
    RPA step to disconnect from OVH
    """
    g_logger.debug("Start of RPA Log-off")
    a_r.timeout(10)  # reset default value
    a_r.click(g_xRoot.find("./config/loginName").text)
    a_r.click("Me déconnecter") if a_lang == Language.FR else a_r.click("Log out")
    a_r.wait(1.0)
    # EndOf rpaLogOff()


def saveUrlToFile(url, path):
    """
    from https://likegeeks.com/downloading-files-using-python/
    and https://docs.python.org/3/library/multiprocessing.html
    """
    # print( f"DEBUG Start mp saveUrlToFile({url}, {path})")
    flx = requests.get(url, stream=True)
    with open(path, "wb") as f:
        for ch in flx:
            f.write(ch)
    # print( f"DEBUG End mp saveUrlToFile({url}, {path})")
    # endOf saveUrlToFile()


class PstFile:
    """
    A .PST file to be downloaded
    """

    def __init__(self, a_email, a_url, a_pathToSave, a_process=None):
        g_logger.debug(f"init Class PstFile({a_email}, XXX, {a_pathToSave} )")
        self._email = a_email
        self._url = a_url
        self._path = a_pathToSave
        self._p = a_process
        self._isDownloadValid = None
        self._size = 0

    def __str__(self):
        s = f"{self._email} - {self._path}"
        return s

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, v):
        self._email = v

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, v):
        self._path = v

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, v):
        self._url = v

    @property
    def process(self):
        return self._p

    @process.setter
    def process(self, v):
        self._process = v

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, v):
        self._size = v

    def download(self):
        """
        download url (cf. https://www.codingem.com/python-download-file-from-url/)
        """
        if self._isDownloadValid:
            return
        g_logger.debug(f"save file '{self._path}' in a new Process.")
        print(
            f"** DEBUG ** download({self._url})"
        )  # Don't save a trace of the URL in a log file, OK on a monitor.
        self._p = Process(target=saveUrlToFile, args=(self._url, self._path))
        self._p.start()
        self._isDownloadValid = None

    def check(self):
        """Check if the pst file is valid
        V1 : by size
        V2 : run scanpst (cf. issue #8)
        """
        if g_xRoot.find("./config/minPstSize") is not None:
            iMinSize = int(g_xRoot.find("./config/minPstSize").text)
        else:
            iMinSize = 20480  # 20Kb
        self._size = os.path.getsize(self._path)
        if self._size < iMinSize:
            self._isDownloadValid = False
        else:
            self._isDownloadValid = True
        g_logger.debug(
            f"Check if PST file is valid : size= {self._size} vs MinSize= {iMinSize} - Valid= {self._isDownloadValid}"
        )
        return self._isDownloadValid

    # EndOf class PstFile


if __name__ == "__main__":
    __spec__ = None  # cf. stackoverflow error multiprocess
    # os.chdir(os.path.split(__file__)[0]) #Change current directory for CRON
    g_logger = pczLogger.PczLogger(LOG_FILENAME, "getpst")
    g_logger.setConfigFromXml(g_xRoot)
    g_sStartingDateTime = datetime.datetime.today().strftime("%d-%m-%Y %H:%M:%S")
    g_logger.info(f"Starting script at {g_sStartingDateTime}")
    g_logger.debug("Configuration file: " + CONFIG_PATH)

    rc = main()

    g_logger.info(f"(Ending script with ReturnCode= {rc})")
    sys.exit(rc)
