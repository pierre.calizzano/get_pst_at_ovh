import rpa as r
import inspect

"""
##https://www.geeksforgeeks.org/how-to-get-list-of-parameters-name-from-a-function-in-python/
"""


# wait()
print("--- r.wait() ---")
print("signature():")
print(inspect.signature(r.wait))
print("getfullargspec():")
print(inspect.getfullargspec(r.wait))

# click()
print("--- r.click() ---")
print("signature():")
print(inspect.signature(r.click))
print("getfullargspec():")
print(inspect.getfullargspec(r.click))
