# README of get_pst_from_ovh.py

## Usage
Download *.PST from OVH.com with RPA automation (tagUI for python)
See [this post](https://pierre.calizzano.net/post/news-2021-12-rpa4pme/) on my web site for more informations (but in french => use GTranslate)

Voir [ce post](https://pierre.calizzano.net/post/news-2021-12-rpa4pme/) sur mon site web pour plus d'infos sur le contexte d'emploi.


## Installation & Configuration 
```
# install python >3.6.4
# install python packages
pip install requests
pip install rpa
# run 
python ./get_pst_at_ovh.py
# run en mode debug
python -m pdb ./get_pst_at_ovh.py
```

Password can be passed by CLI argument, <password> tag in XML config file (/!\ security risk), input keybord at startup (best).

### Configuration file (template)
```
<root>
    	<config>
            <loginName>John Doe</loginName>
            <loginId>xy123456-ovh</loginId>
            <linkExchangeId>hosted-xy123456-1</linkExchangeId>
            <pathToSave>C:/mnt/USB_1TO/SauvegardesExchangeOVH</pathToSave>
    	</config>
    	<comptes> <!-- sort by increasing size - classer par taille croissante -->
    		<compte>
    			<ovh_email>service.rh@exemple.fr</ovh_email>
    			<ovh_filter>service.rh@</ovh_filter>
    			<file_name>service_rh</file_name>
    		</compte>
    <!--
    		<compte>
    			<ovh_email></ovh_email>
    			<ovh_filter></ovh_filter>
    			<file_name></file_name>
    		</compte>
    -->
    	</comptes>
</root>
```
