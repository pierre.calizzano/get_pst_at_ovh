#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    test code for download with multi-process
"""
import sys
import os
import time
import datetime
import platform
import enum
import rpa as r
import xml.etree.ElementTree as xet
import requests

# from multiprocessing.pool import ThreadPool
from multiprocessing import Process


def saveUrlToFile(url, path):
    """
    from https://likegeeks.com/downloading-files-using-python/
    and https://docs.python.org/3/library/multiprocessing.html
    """
    r = requests.get(url, stream=True)
    with open(path, "wb") as f:
        for ch in r:
            f.write(ch)
    # endOf saveUrlToFile()


def main():
    try:
        rc = -1
        print("DEBUG - Starting script")
        arProcess = []

        TS_FORMAT = "%Y%m%d_%H%M%S"
        sDateTime = datetime.datetime.today().strftime(TS_FORMAT)
        arFiles = [
            "https://pierre.calizzano.net/medias/cyberSec/Pres.CyberSec_v2.pdf",
            "https://pierre.calizzano.net/medias/cyberSec/anssi_bonnes_pratiques_secinfo_poster_a1_anssi.pdf",
            "https://pierre.calizzano.net/medias/cyberSec/anssi_bonnes_pratiques_secinfo_poster_a1_anssi.pdf",
            "https://pierre.calizzano.net/medias/cyberSec/anssi_bonnes_pratiques_secinfo_poster_a1_anssi.pdf",
            "https://pierre.calizzano.net/medias/cyberSec/anssi_bonnes_pratiques_secinfo_poster_a1_anssi.pdf",
        ]
        i = 0
        for sUrl in arFiles:
            # sFileName = sDateTime + sUrl
            i = i + 1
            sFileName = "{0}_{1}.txt".format(sDateTime, i)
            print("DEBUG - save file '{0}'.\n".format(sFileName))
            # saveUrlToFile(urlPst, sFileName)
            p = Process(target=saveUrlToFile, args=(sUrl, sFileName))
            p.start()
            arProcess.append(p)
        # THE END
        print("\nDEBUG - Attendre la fin des download...\n")
        for p in arProcess:
            p.join()
            print("DEBUG - Un process est terminé\n")
        input("THE END - Press Enter to continue...")

        ## Fin du script
        rc = 0
    except Exception as e:
        print("EXCEPTION: Fatale error !")
        rc = -1
    finally:
        # r.close()
        print("Ending script with ReturnCode= {0}".format(rc))
        return rc
    # EndOf main()


if __name__ == "__main__":
    __spec__ = None  # cf.
    # os.chdir(os.path.split(__file__)[0]) #Change current directory for CRON
    rc = main()
    exit(rc)
